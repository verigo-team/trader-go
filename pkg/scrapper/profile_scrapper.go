package scrapper

import (
	"bitbucket.org/verigo-team/logger"
	"bitbucket.org/verigo-team/logger/closer"
	"bitbucket.org/verigo-team/trader/pkg/models"
	"bitbucket.org/verigo-team/trader/pkg/settings"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
)

var (
	generalInfoURL = func(assetId string) string {
		return fmt.Sprintf(`%s/equities/%s-company-profile`, settings.GetSettings().InvestingCom.Url, assetId)
	}
	stocksAndCapitalizationInfoURL = func(assetId string) string {
		return fmt.Sprintf(`%s/equities/%s`, settings.GetSettings().InvestingCom.Url, assetId)
	}
)

func (s *Scrapper) ScrapProfile(id string, name string, index string) models.AssetProfile {
	profile := models.AssetProfile{
		Id:    id,
		Name:  name,
		Index: index,
	}

	s.scrapAndFillGeneralInfo(&profile)
	s.scrapAndFillStocksAndCapitalization(&profile)

	return profile
}

func (s *Scrapper) scrapAndFillGeneralInfo(profile *models.AssetProfile) {
	infoUrl := generalInfoURL(profile.Id)
	resp, err := s.httpClient.Get(infoUrl)
	logger.Fatal(err, `load company profile error`)
	defer closer.TryClose(resp.Body.Close)

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	logger.Fatal(err, `read company profile error`)

	doc.Find(`.companyProfileHeader > div`).Each(func(i int, selection *goquery.Selection) {
		switch i {
		case 0:
			profile.Department = selection.Find(`a`).Text()
			break
		case 1:
			profile.Sector = selection.Find(`a`).Text()
			break
		case 2:
			employees, err := strconv.ParseUint(selection.Find(`p`).Text(), 10, 32)
			logger.Fatal(err, `parse employees count error`)
			profile.Employees = uint32(employees)
			break
		case 3:
			profile.Type = selection.Find(`p`).Text()
			break
		}
	})
}

func (s *Scrapper) scrapAndFillStocksAndCapitalization(profile *models.AssetProfile) {
	resp, err := s.httpClient.Get(stocksAndCapitalizationInfoURL(profile.Id))
	logger.Fatal(err, `load company profile error`)
	defer closer.TryClose(resp.Body.Close)

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	logger.Fatal(err, `read company info error`)

	doc.Find(`.overviewDataTableWithTooltip > div`).Each(func(i int, selection *goquery.Selection) {
		switch i {
		case 13:
			stocksStr := selection.Find(`span.float_lang_base_2`).Text()
			stocksStr = strings.Replace(stocksStr, `.`, ``, -1)

			stocksNum, err := strconv.ParseUint(stocksStr, 10, 32)
			logger.Fatal(err, `load company stocks error`)

			profile.Stocks = uint64(stocksNum)
			break
		case 7:
			capitalizationStr := selection.Find(`span.float_lang_base_2`).Text()
			profile.Capitalization = uint64(parseBigNumber(capitalizationStr))
			break
		}
	})
}
