package saver

import (
	"bitbucket.org/verigo-team/logger/closer"
	"time"

	"bitbucket.org/verigo-team/logger"
	"bitbucket.org/verigo-team/trader/pkg/models"
	"bitbucket.org/verigo-team/trader/pkg/settings"

	"github.com/jmoiron/sqlx"
)

type Saver struct {
	db *sqlx.DB
}

func NewSaver(dbSettings settings.DBSettings) *Saver {
	db := openDB(dbSettings)

	return &Saver{
		db: db,
	}
}

func (s *Saver) SaveDailyAssets(profile models.AssetProfile, history []models.HistoricalData) {
	tx := s.db.MustBegin()

	_, err := tx.Exec(`
	INSERT INTO assets 
		(id, name, index, department, employees, type, sector, stocks, capitalization)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
	`,
		profile.Id,
		profile.Name,
		profile.Index,
		profile.Department,
		profile.Employees,
		profile.Type,
		profile.Sector,
		profile.Stocks,
		profile.Capitalization,
	)
	logger.Fatal(err, `insert assets error`)

	err = tx.Commit()
	logger.Fatal(err, `at inserting assets transaction commit error`)

	tx = s.db.MustBegin()

	stmt, err := tx.Prepare(`
	INSERT INTO default.assets_stocks_daily 
		(asset_id, dt, price, min_value, max_value, open_value, value, delta) 
	VALUES (?, ?, ?, ?, ?, ?, ?, ?)
	`)
	logger.Fatal(err, `insert assets stocks daily error`)
	defer closer.TryClose(stmt.Close)

	for _, item := range history {
		date, err := time.Parse("02.01.2006", item.Date)
		logger.Fatal(err, `at inserting assets stocks daily, parse date error`)

		if _, err = stmt.Exec(
			profile.Id,
			date,
			item.Price,
			item.Min,
			item.Max,
			item.Open,
			item.Value,
			item.Delta,
		); err != nil {
			logger.Fatal(err, `at inserting assets stocks daily, insert row error`)
		}
	}

	err = tx.Commit()
	logger.Fatal(err, `at inserting assets stocks daily transaction commit error`)
}
