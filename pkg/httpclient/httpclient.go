package httpclient

import (
	"bitbucket.org/verigo-team/logger"
	"net/http"
)

type HttpClient struct {
	client *http.Client
}

func NewHttpClient() *HttpClient {
	return &HttpClient{
		client: &http.Client{},
	}
}

func (c *HttpClient) Get(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	logger.Fatal(err, `request building error`)

	req.Header.Set("User-Agent", "Golang_Spider_Bot/3.0")

	return c.client.Do(req)
}

func (c *HttpClient) Do(req *http.Request) (*http.Response, error) {
	req.Header.Set("User-Agent", "Golang_Spider_Bot/3.0")

	return c.client.Do(req)
}
