
-- Clickhouse schemes for dataset storing

CREATE TABLE IF NOT EXISTS assets (
    id String,
    name String,
    index String,
    department String,
    employees UInt32,
    type String,
    sector String,
    stocks UInt64,
    capitalization UInt64
)
ENGINE = MergeTree
ORDER BY id

CREATE TABLE IF NOT EXISTS assets_stocks_daily (
    asset_id String,
    dt Date,
    price Float64,
    min_value Float64,
    max_value Float64,
    open_value Float64,
    value Float64,
    delta Float64
)
ENGINE = MergeTree
ORDER BY asset_id
