package scrapper

import "bitbucket.org/verigo-team/trader/pkg/httpclient"

type Scrapper struct {
	httpClient *httpclient.HttpClient
}

func NewScrapper() *Scrapper {
	return &Scrapper{
		httpClient: httpclient.NewHttpClient(),
	}
}
