package scrapper

import (
	"bitbucket.org/verigo-team/logger"
	"bitbucket.org/verigo-team/trader/pkg/models"
	"bitbucket.org/verigo-team/trader/pkg/settings"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strings"
)

var componentsURL = func(indexName string) string {
	investingComUrl := settings.GetSettings().InvestingCom.Url
	return fmt.Sprintf(`%s/indices/investing.com-%s-components`,
		investingComUrl,
		indexName,
	)
}

func (s *Scrapper) ScrapIndexComponents(indexName string) []models.IndexComponent {
	url := componentsURL(indexName)
	resp, err := s.httpClient.Get(url)
	logger.Fatal(err, `load index component error`)
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	logger.Fatal(err, `read index component error`)

	var components []models.IndexComponent

	doc.Find(`#marketInnerContent table.crossRatesTbl tr`).Each(func(i int, selection *goquery.Selection) {
		indexComponent := models.IndexComponent{Index: indexName}
		selection.Find(`td`).Each(func(ii int, selection *goquery.Selection) {
			if ii != 1 {
				return
			}

			link := selection.Find(`a`)

			componentUrl, _ := link.Attr(`href`)
			indexComponent.Id = strings.Replace(componentUrl, `/equities/`, ``, -1)
			indexComponent.Name = link.Text()

			components = append(components, indexComponent)
		})

	})

	return components
}
