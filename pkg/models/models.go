package models

// AssetProfile represents asset profile
type AssetProfile struct {
	Id             string
	Name           string
	Index          string
	Department     string
	Sector         string
	Employees      uint32
	Capitalization uint64
	Type           string
	Stocks         uint64
}

// HistoricalData represents assets historical data (stocks changing day by day)
type HistoricalData struct {
	Date  string
	Price float64
	Min   float64
	Max   float64
	Open  float64
	Value float64
	Delta float64
}

// IndexComponent represents components of indexes (for example Apple of S&P500)
type IndexComponent struct {
	// Id for url generation
	Id    string
	// Name just name
	Name  string
	Index string
}
