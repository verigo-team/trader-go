package scrapper

import (
	"bitbucket.org/verigo-team/logger"
	"regexp"
	"strconv"
	"strings"
)

var onlyNumber = regexp.MustCompile(`[-0-9.,BMK]*`)

func parseBigNumber(numberOriginal string) float64 {
	number := strings.Replace(numberOriginal, `.`, ``, -1)
	number = strings.Replace(numberOriginal, `,`, `.`, -1)
	number = onlyNumber.FindAllString(number, -1)[0]
	if strings.HasSuffix(number, `B`) {
		number = strings.Replace(number, `B`, ``, -1)
		numberFloat, err := strconv.ParseFloat(number, 32)
		if err != nil {
			logger.Fatal(err, `parse billion error`)
		}

		return numberFloat * 1000000000
	} else if strings.HasSuffix(number, `M`) {
		number = strings.Replace(number, `M`, ``, -1)
		numberFloat, err := strconv.ParseFloat(number, 32)
		if err != nil {
			logger.Fatal(err, `parse million error`)
		}

		return numberFloat * 1000000
	} else if strings.HasSuffix(number, `K`) {
		number = strings.Replace(number, `K`, ``, -1)
		numberFloat, err := strconv.ParseFloat(number, 32)
		if err != nil {
			logger.Fatal(err, `parse thousand error`)
		}

		return numberFloat * 1000
	} else if number == `-` {
		return 0
	} else {
		number = strings.Replace(number, `%`, ``, -1)
		numberFloat, err := strconv.ParseFloat(number, 32)
		if err != nil {
			logger.Fatal(err, `parse percents error`)
		}

		return numberFloat
	}
}
