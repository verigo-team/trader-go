package main

import (
	"bitbucket.org/verigo-team/trader/pkg/saver"
	"bitbucket.org/verigo-team/trader/pkg/scrapper"
	"bitbucket.org/verigo-team/trader/pkg/settings"
	"log"
)

var Settings settings.Settings

// Load dataset for back testing
func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	settings.Init(Settings)

	scrapperInstance := scrapper.NewScrapper()
	saverInstance := saver.NewSaver(settings.GetSettings().DB)
	snp500Components := scrapperInstance.ScrapIndexComponents(`us-500`)

	for _, component := range snp500Components {
		profile := scrapperInstance.ScrapProfile(component.Id, component.Name, component.Index)
		history := scrapperInstance.ScrapHistory(component.Id)

		saverInstance.SaveDailyAssets(profile, history)
	}
}
