package saver

import (
	"bitbucket.org/verigo-team/trader/pkg/settings"
	"fmt"
	"github.com/kshvakov/clickhouse"

	"bitbucket.org/verigo-team/logger"

	"github.com/jmoiron/sqlx"
	_ "github.com/kshvakov/clickhouse"
)

func openDB(settings settings.DBSettings) *sqlx.DB {
	dataSourceName := fmt.Sprintf("tcp://%s:%d?dbname=%s&user=%s&password=%s&sslmode=disable",
		settings.Host,
		settings.Port,
		settings.Database,
		settings.User.Name,
		settings.User.Password,
	)

	connect, err := sqlx.Connect("clickhouse", dataSourceName)
	logger.Fatal(err, `db connect error`)

	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		}
		logger.Fatal(err, `db ping error`)
	}

	return connect
}
