package scrapper

import (
	"bitbucket.org/verigo-team/logger"
	"bitbucket.org/verigo-team/logger/closer"
	"bitbucket.org/verigo-team/trader/pkg/models"
	"bitbucket.org/verigo-team/trader/pkg/settings"
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var (
	historicalDataAjaxUrl = func() string {
		return fmt.Sprintf(`%s/instruments/HistoricalDataAjax`, settings.GetSettings().InvestingCom.Url)
	}

	historicalDataPageUrl = func(componentId string) string {
		return fmt.Sprintf(`%s/equities/%s-historical-data`, settings.GetSettings().InvestingCom.Url, componentId)
	}

	currIdPattern = regexp.MustCompile(`pairId: (\d+)`)
	smlIdPattern  = regexp.MustCompile(`smlId: (\d+)`)
)

func (s *Scrapper) ScrapHistory(componentId string) []models.HistoricalData {
	currId, smlId := s.getLoadHistoricalDataKey(componentId)

	historyAjax := s.loadHistoryAjax(currId, smlId)
	defer closer.TryClose(historyAjax.Close)

	doc, err := goquery.NewDocumentFromReader(historyAjax)
	logger.Fatal(err, `load history error`)

	var result []models.HistoricalData
	doc.Find("table.historicalTbl tbody tr").Each(func(i int, s *goquery.Selection) {
		var historyData = models.HistoricalData{}
		s.Find("td").Each(func(ii int, ss *goquery.Selection) {
			switch ii {
			case 0:
				historyData.Date = ss.Text()
				break
			case 1:
				historyData.Price = parseBigNumber(ss.Text())
				break
			case 2:
				historyData.Open = parseBigNumber(ss.Text())
				break
			case 3:
				historyData.Max = parseBigNumber(ss.Text())
				break
			case 4:
				historyData.Min = parseBigNumber(ss.Text())
				break
			case 5:
				historyData.Value = parseBigNumber(ss.Text())
				break
			case 6:
				historyData.Delta = parseBigNumber(ss.Text())
				break
			}
		})
		result = append(result, historyData)
	})

	return result
}

func (s *Scrapper) getLoadHistoricalDataKey(componentId string) (string, string) {
	pageUrl := historicalDataPageUrl(componentId)

	resp, err := s.httpClient.Get(pageUrl)
	logger.Fatal(err, `get historical data load key error`)
	defer closer.TryClose(resp.Body.Close)

	respBytes, err := ioutil.ReadAll(resp.Body)
	logger.Fatal(err, `read historical data page error`)

	respStr := string(respBytes)

	currIdSubMatches := currIdPattern.FindStringSubmatch(respStr)
	if len(currIdSubMatches) < 1 {
		logger.Fatal(errors.New(`curr id not founded`), `load historical data key (curr id) error`)
	}

	smlIdMatches := smlIdPattern.FindStringSubmatch(respStr)
	if len(smlIdMatches) < 1 {
		logger.Fatal(errors.New(`sml id not founded`), `load historical data key (sml id) error`)
	}

	return currIdSubMatches[1], smlIdMatches[1]
}

func (s *Scrapper) loadHistoryAjax(currId string, smlId string) io.ReadCloser {
	form := url.Values{}
	form.Add("curr_id", currId)
	form.Add("smlID", smlId)
	form.Add("st_date", `14/07/2005`)
	form.Add("end_date", `14/07/2018`)
	form.Add("interval_sec", `Daily`)
	form.Add("sort_col", `date`)
	form.Add("sort_ord", `DESC`)
	form.Add("action", `historical_data`)

	req, err := http.NewRequest(
		"POST",
		historicalDataAjaxUrl(),
		strings.NewReader(form.Encode()),
	)
	logger.Fatal(err, `history request error`)

	req.Header.Add("X-Requested-With", `XMLHttpRequest`)
	req.Header.Add("Content-Type", `application/x-www-form-urlencoded`)

	resp, err := s.httpClient.Do(req)
	logger.Fatal(err, `history request do error`)

	return resp.Body
}
