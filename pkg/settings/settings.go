package settings

import (
	"bitbucket.org/verigo-team/logger"
	"errors"
	"github.com/jinzhu/configor"
)

var defaultConfig *Settings

type Settings struct {
	DB           DBSettings
	InvestingCom InvestingCom
}

func Init(config Settings) {
	if err := configor.Load(&config); err != nil {
		logger.Fatal(err, `config load error`)
	}
	defaultConfig = &config
}

func GetSettings() *Settings {
	if defaultConfig == nil {
		logger.Fatal(errors.New(`setting was not initialized`), `get settings error`)
	}
	return defaultConfig
}

type DBSettings struct {
	Host     string `required:"true" env:"db_host"`
	Port     int    `default:"9000" env:"db_port"`
	Database string `default:"default" env:"db_name"`
	User     struct {
		Name     string `default:"default" env:"db_user_name"`
		Password string `required:"true" env:"db_user_password"`
	}
}

type InvestingCom struct {
	Url string `default:"https://ru.investing.com"`
}
